function main(params) {
    return {success: true, html: document.body.innerHTML};
}

document.getElementById( "bbCode" ).addEventListener( "click", function() {
    this.setSelectionRange(0, this.value.length);
} );

document.getElementById( "openMaturePetButton" ).addEventListener( "click", function( tab ) {
    chrome.tabs.executeScript({
        code: '(' + function() {
            return {success: true, html: document.body.innerHTML};
        } + ')();'
    }, function(results) {
        var htmlContent = results[0].html;
        var parser = new DOMParser();
        var pseudoDocument = parser.parseFromString( htmlContent, "text/html" );

        var petImage = pseudoDocument.getElementById("viewpetsform").getElementsByTagName( "img" )[0].getAttribute('src');

        petImage = petImage.replace( "maturity=", "maturity=99" );

        window.open("https://www.sylestia.com" + petImage);
    } );
} );

document.getElementById( "generateCodeButton" ).addEventListener( "click", function( tab ) {
    var url;
    chrome.tabs.query({currentWindow: true, active: true}, function(tabs){
        url = tabs[0].url;
    });
    chrome.tabs.executeScript({
        code: '(' + function() {
            return {success: true, html: document.body.innerHTML};
        } + ')();'
    }, function(results) {
        var htmlContent = results[0].html;
        var parser = new DOMParser();
        var pseudoDocument = parser.parseFromString( htmlContent, "text/html" );
        
        var petImage = pseudoDocument.getElementById("viewpetsform").getElementsByTagName( "img" )[0].getAttribute('src');

        var divList = pseudoDocument.getElementsByTagName( "div" );
        var attributeListing;
        for (var i = divList.length - 1; i >= 0; i--) {
            if (divList[i].textContent.indexOf( "Gene One" ) > - 1) {
                attributeListing = divList[i];
                break;
            }
        }

        var modifierType = {
            geneOne : "",
            geneTwo : "",
            geneThree : "",
            mutationOne : "",
            mutationTwo : "",
            mutationThree : ""
        }

        var attributeContent = attributeListing.textContent;
        var attributesObject = {
            gender : "",
            children : "",
            geneOne : "",
            geneTwo : "",
            geneThree : "",
            mutationOne : "",
            mutationTwo : "",
            mutationThree : "",
            isExclusive : false,
            exclusiveType : "",
            species : "",
            toString : function( index ) {
                var carryValue = document.getElementById( "carryTag" ).value;
                var visibleValue = document.getElementById( "visibleTag" ).value;

                if( carryValue == "" ) {
                    carryValue = "i";
                }
                if( visibleValue == "" ) {
                    visibleValue = "b";
                }
                if( modifierType[index] == "carry" ) {
                    return "[" + carryValue + "]" + this[index] + "[/" + carryValue + "]";
                } else if( modifierType[index] == "visible" ) {
                    return "[" + visibleValue + "]" + this[index] + "[/" + visibleValue + "]";
                }

                return this[index];
            } 
        };
        var keyIndexes = [ 
            attributeContent.indexOf( "Gender:" ) + 8,
            attributeContent.indexOf( "Children:" ) + 10,
            attributeContent.indexOf( "Gene One:" ) + 10, 
            attributeContent.indexOf( "Gene Two:" ) + 10,
            attributeContent.indexOf( "Gene Three:" ) + 12 ,
            attributeContent.indexOf( "Mutation One:" ) + 14,
            attributeContent.indexOf( "Mutation Two:" ) + 14,
            attributeContent.indexOf( "Mutation Three:" ) + 16,
            attributeContent.indexOf( "This is an Exclusive Sylesti" )
        ];

        if( document.getElementById( "showSpecies" ).checked ) {
            attributesObject.species = attributeContent.substring( attributeContent.indexOf( "Species:" ) + 9, attributeContent.indexOf( "\n", attributeContent.indexOf( "Species:" ) + 9 ) );
        }

        attributesObject.gender = attributeContent.substring( keyIndexes[0], attributeContent.indexOf( "\n", keyIndexes[0] ) );
        attributesObject.children = attributeContent.substring( keyIndexes[1], attributeContent.indexOf( "\n", keyIndexes[1] ) );
        attributesObject.geneOne = attributeContent.substring( keyIndexes[2], attributeContent.indexOf( "\n", keyIndexes[2] ) );
        attributesObject.geneTwo = attributeContent.substring( keyIndexes[3], attributeContent.indexOf( "\n", keyIndexes[3] ) );
        attributesObject.geneThree = attributeContent.substring( keyIndexes[4], attributeContent.indexOf( "\n", keyIndexes[4] ) );
        attributesObject.mutationOne = attributeContent.substring( keyIndexes[5], attributeContent.indexOf( "\n", keyIndexes[5] ) );
        attributesObject.mutationTwo = attributeContent.substring( keyIndexes[6], attributeContent.indexOf( "\n", keyIndexes[6] ) );
        attributesObject.mutationThree = attributeContent.substring( keyIndexes[7], attributeContent.indexOf( "\n", keyIndexes[7] ) );
        attributesObject.isExclusive = keyIndexes[8] > -1;
        if( keyIndexes[8] > -1 ) {
            attributesObject.exclusiveType = attributeContent.substring( keyIndexes[8] + 28, attributeContent.indexOf( "\n", keyIndexes[8] ) );
        }

        function assignModifierType( listing, objectValue, key ) {
            if( objectValue == "None" ) {
                modifierType[key] = "none";
            } else if( listing.innerHTML.indexOf( objectValue + "</i>" ) > -1 ) {
                modifierType[key] = "carry";
            } else {
                modifierType[key] = "visible";
            }
        }

        assignModifierType( attributeListing, attributesObject.geneOne, "geneOne" );
        assignModifierType( attributeListing, attributesObject.geneTwo, "geneTwo" );
        assignModifierType( attributeListing, attributesObject.geneThree, "geneThree" );
        assignModifierType( attributeListing, attributesObject.mutationOne, "mutationOne" );
        assignModifierType( attributeListing, attributesObject.mutationTwo, "mutationTwo" );
        assignModifierType( attributeListing, attributesObject.mutationThree, "mutationThree" );

        var bbCodeField = document.getElementById( "bbCode" );
        bbCodeField.value="[img=" + url + "]https://www.sylestia.com" + petImage + "[/img]\n\n";
        if( attributesObject.isExclusive ) {
            var color = document.getElementById( "colorValue" ).value;
            if( color == "" ) {
                color = "0000FF";
            }
            bbCodeField.value+="[b][color=" + color + "]" + attributesObject.exclusiveType + "[/color][/b]\n";
        }
        if( document.getElementById( "showSpecies" ).checked ) {
            bbCodeField.value += attributesObject.species + "\n";
        }
        bbCodeField.value += "Gender: " + attributesObject.gender + "\n";
        bbCodeField.value += "Children: " + attributesObject.children + "\n";
        bbCodeField.value += "Gene One: " + attributesObject.toString( "geneOne" ) + "\n";
        bbCodeField.value += "Gene Two: " + attributesObject.toString( "geneTwo" ) + "\n";
        bbCodeField.value += "Gene Three: " + attributesObject.toString( "geneThree" ) + "\n";
        bbCodeField.value += "Mutation One: " + attributesObject.toString( "mutationOne" ) + "\n";
        bbCodeField.value += "Mutation Two: " + attributesObject.toString( "mutationTwo" ) + "\n";
        bbCodeField.value += "Mutation Three: " + attributesObject.toString( "mutationThree" ) + "\n";
    });
} );